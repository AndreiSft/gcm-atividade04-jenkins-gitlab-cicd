package br.ucsal.bes20191.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20191.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class ComandaDao {

	private static final String MENS_COMANDA_NAO_ENCONTRADA = "Comanda n�o encontrada (c�digo = %d).";
	
	public static List<Comanda> itens = new ArrayList<>();

	public static void incluir(Comanda comanda) {
		System.out.println("INCLUIR COMANDA!");
		itens.add(comanda);
	}

	public static Comanda obterPorCodigo(Integer codigo) throws RegistroNaoEncontrado {
		for (Comanda comanda : itens) {
			if (comanda.getCodigo().equals(codigo)) {
				return comanda;
			}
		}
		throw new RegistroNaoEncontrado(String.format(MENS_COMANDA_NAO_ENCONTRADA, codigo));
	}

}
