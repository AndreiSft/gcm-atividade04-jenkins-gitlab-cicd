package br.ucsal.bes20191.testequalidade.restaurante.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20191.testequalidade.restaurante.exception.RegistroNaoEncontrado;

public class MesaDao {

	private static final String MESA_NAO_ENCONTRADA = "Mesa não encontrada (número = %d).";
	
	public static List<Mesa> itens = new ArrayList<>();

	public static void incluir(Mesa mesa) {
		itens.add(mesa);
	}

	public static Mesa obterPorNumero(Integer numero) throws RegistroNaoEncontrado {
		System.out.println("OBTER MESA POR N�MERO!");
		for (Mesa mesa : itens) {
			if (mesa.getNumero().equals(numero)) {
				return mesa;
			}
		}
		throw new RegistroNaoEncontrado(String.format(MESA_NAO_ENCONTRADA, numero));
	}

}
