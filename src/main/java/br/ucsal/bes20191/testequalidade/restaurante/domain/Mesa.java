package br.ucsal.bes20191.testequalidade.restaurante.domain;

import br.ucsal.bes20191.testequalidade.restaurante.enums.SituacaoMesaEnum;

public class Mesa {

	public SituacaoMesaEnum situacao = SituacaoMesaEnum.LIVRE;

	public Integer numero;

	public Mesa(Integer numero) {
		super();
		this.numero = numero;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public SituacaoMesaEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Mesa [situacao=" + situacao + ", numero=" + numero + "]";
	}

}
