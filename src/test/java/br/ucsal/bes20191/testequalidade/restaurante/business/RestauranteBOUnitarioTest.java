package br.ucsal.bes20191.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20191.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20191.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MesaDao.class, ComandaDao.class })
public class RestauranteBOUnitarioTest {

	/**
	 * Metodo a ser testado: public static Integer abrirComanda(Integer numeroMesa)
	 * throws RegistroNaoEncontrado, MesaOcupadaException. Verificar se a abertura
	 * de uma comanda para uma mesa livre apresenta sucesso. Lembre-se de verificar
	 * a chamada ao ComandaDao.incluir(comanda).
	 * 
	 * @throws Exception
	 */

	@Test
	public void abrirComandaMesaLivre() throws Exception {

		PowerMockito.mockStatic(ComandaDao.class);

		PowerMockito.mockStatic(MesaDao.class);
		Integer numeroMesa = 646;
		Mesa mesa = new Mesa(numeroMesa);
		mesa.setSituacao(SituacaoMesaEnum.LIVRE);
		PowerMockito.when(MesaDao.obterPorNumero(numeroMesa)).thenReturn(mesa);

//		Whitebox.invokeMethod(RestauranteBO.class, "abrirComanda", numeroMesa);
		RestauranteBO.abrirComanda(numeroMesa);

		PowerMockito.verifyStatic(ComandaDao.class);
		ComandaDao.incluir(Mockito.any(Comanda.class));
	}

}
